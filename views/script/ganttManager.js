  /**
   * In this File we are taking back data from the dataBase
   * So we display them into the page web using google diagram chart
   * Here we are making some data modifications to display them
   * correctly
   */
  'use strict';

  //Google gantt diagram library call
  google.charts.load('current', {
    'packages': ['gantt']
  });
  google.charts.setOnLoadCallback(drawChart);

  //Diagram properties
  function drawChart() {

    //Diagram parameters
    const data = new google.visualization.DataTable();
    data.addColumn('string', 'Task ID');
    data.addColumn('string', 'Task Name');
    data.addColumn('string', 'Resource');
    data.addColumn('date', 'Start Date');
    data.addColumn('date', 'End Date');
    data.addColumn('number', 'Duration');
    data.addColumn('number', 'Percent Complete');
    data.addColumn('string', 'Dependencies');

    /**
     * Received a new task event
     */
    socket.on('clientEmitNewTask', function (newTask) {
      //Hide the modal after the creation
      const modal            = document.getElementsByClassName('modal');
      modal[1].style.display = "none";

      //Reset fields after creation
      document.getElementById("createTaskName").value  = "";
      document.getElementById("createStartDate").value = "";
      document.getElementById("createEndDate").value   = "";
      document.getElementById("createPercent").value   = 0;
      document.getElementById("createDesc").value      = "";

      //Define tasks variables
      let taskName        = newTask.projects[0].task[0].name;                                       //Catch the DB name
      let resource        = newTask.projects[0].task[0].resources[0];                               //Catch DB resources
      let startDateParse  = newTask.projects[0].task[0].start.split("-");                           //Change Date format
      let endDateParse    = newTask.projects[0].task[0].end.split("-");                             //Change Date format
      let startDate       = new Date(startDateParse[0], startDateParse[1] - 1, startDateParse[2]);  //Define the date
      let endDate         = new Date(endDateParse[0], endDateParse[1] - 1, endDateParse[2]);        //Define the date
      let duration        = null;                                                                   //Will be filled automatically thanks to the start and the end dates 
      let percentComplete = newTask.projects[0].task[0].percentageProgress;                         //Catch DB percentage
      let dependencies    = null;                                                                   //We won't use this parameter

      //Tab creation with different tasks fields
      //Change percentage format (string to number)
      let taskType = [taskName, taskName, resource, startDate, endDate, duration, parseInt(percentComplete, 10), dependencies];

      //Adding tasktype in the diagram parameter function
      data.addRows([
        //We only display the taskType part then we will have an error (i is not a defined parameter)
        taskType
      ]);

      //Define the diagram sizes
      const options = {
        //Increment the size of the diagram for every new task
        height: 450,
        //Default Task Height
        gantt: {
          trackHeight: 30
        }
      };

      //Make a diagram link with html
      const chart = new google.visualization.Gantt(document.getElementById('ganttDiagram'));
      //Display the diagram with data and options parameters
      chart.draw(data, options);
    })

    /**
     * Received a new task
     */
    socket.on('clientEmitListTask', function (newListTask) {
      //Get all paragraph on modal div to receive task  
      let taskNotFound           = document.getElementById("taskNotFound");
      let nameService            = document.getElementById("nameServiceEmptyList");
      let projectName            = document.getElementById("projectNameEmptyList");
      let projectDesc            = document.getElementById("projectDescEmptyList");
      let daysOff                = document.getElementById("daysOffEmptyList");
      let WorkingHourStart       = document.getElementById("workingHourStartEmptyList");
      let workingHourEnd         = document.getElementById("workingHourEndEmptyList");
      let taskResources          = document.getElementById("taskResourcesEmptyList");
      let taskName               = document.getElementById("taskNameEmptyList");
      let taskDesc               = document.getElementById("taskDescEmptyList");
      let taskStart              = document.getElementById("taskStartEmptyList");
      let taskEnd                = document.getElementById("taskEndEmptyList");
      let taskPercentageProgress = document.getElementById("taskPercentageProgressEmptyList");
      let resourcesName          = document.getElementById("resourcesNameEmptyList");
      
      //Display fields if the task exist
      if (newListTask.length !== 0) {
        taskNotFound.innerHTML = "";
        nameService.innerHTML  = "Name service : " + newListTask["0"].nameService;
        projectName.innerHTML  = "Project name : " + newListTask["0"].projects["0"].name;
        projectDesc.innerHTML  = "Project description : " + newListTask["0"].projects["0"].desc;
        //daysOff.innerHTML = "Days off : ";
        WorkingHourStart.innerHTML       = "Working hour start : " + newListTask["0"].projects["0"].workingHours.start + "h00" ;
        workingHourEnd.innerHTML         = "Working hour end : " + newListTask["0"].projects["0"].workingHours.end + "h00" ;
        taskResources.innerHTML          = "Resources : " + newListTask["0"].projects["0"].task["0"].resources["0"] ;
        taskName.innerHTML               = "Task name : " + newListTask["0"].projects["0"].task["0"].name;
        taskDesc.innerHTML               = "Task description : " + newListTask["0"].projects["0"].task["0"].desc;
        taskStart.innerHTML              = "Task start date : " + moment(newListTask["0"].projects["0"].task["0"].start).format("LL");
        taskEnd.innerHTML                = "Task end date : " + moment(newListTask["0"].projects["0"].task["0"].end).format("LL");
        taskPercentageProgress.innerHTML = "Task percentage progress : " +  newListTask["0"].projects["0"].task["0"].percentageProgress + "%";
        resourcesName.innerHTML          = "Task resources : " + newListTask["0"].projects["0"].resources["0"].name;
        console.log(newListTask);
      } //else reset displayed fields an display an error message
      else{
        taskNotFound.innerHTML = "404 Task not found ! " + "&#128557;";
        nameService.innerHTML  = "";
        projectName.innerHTML  = "";
        projectDesc.innerHTML  = "";
        //daysOff.innerHTML = "Days off : ";
        WorkingHourStart.innerHTML       = "";
        workingHourEnd.innerHTML         = "";
        taskResources.innerHTML          = "";
        taskName.innerHTML               = "";
        taskDesc.innerHTML               = "";
        taskStart.innerHTML              = "";
        taskEnd.innerHTML                = "";
        taskPercentageProgress.innerHTML = "";
        resourcesName.innerHTML          = "";
      }
      
    })

    socket.on('UpdateNotFound', function()
    {
      document.getElementById("updateTaskNotFound");
      updateTaskNotFound.innerHTML = "404 Task not found ! " + "&#128557;";
    })

    socket.on('updateTask', function()
    {
      document.getElementById("updateTaskNotFound");
      updateTaskNotFound.innerHTML = "";
    })


    /**
     * Get task at connection or reload
     */
    socket.on('getTasks', function(taskDocs)
    {
        //Make a loop to display all tasks
        for(let i = 0; i < taskDocs.length; i++)
        {  
          //Define tasks variables
          //As the task creation there is format changes for some data
          let taskName        = taskDocs[i.toString()].projects[0].task[0].name;
          let resource        = taskDocs[i.toString()].projects[0].task[0].resources[0];
          let startDateParse  = taskDocs[i.toString()].projects[0].task[0].start.split("-");
          let endDateParse    = taskDocs[i.toString()].projects[0].task[0].end.split("-");
          let startDate       = new Date(startDateParse[0], startDateParse[1] - 1, startDateParse[2]);
          let endDate         = new Date(endDateParse[0], endDateParse[1] - 1, endDateParse[2]);        //Will be filled automatically thanks to the start and the end dates 
          let percentComplete = taskDocs[i.toString()].projects[0].task[0].percentageProgress;          //We won't use this parameter

          //Tab creation with different tasks fields
          let taskType = [taskName, taskName, resource, startDate, endDate, null, parseInt(percentComplete, 10), null];

          data.addRows([
            //We only display the taskType part then we will have an error (i is not a defined parameter)
            taskType
          ]);
        }
        //Leave the loop and display fields

        //Define the diagram sizes
        const options = {
          //Increment the size of the diagram for every new task
          height: 450,
          //Default Task Height
          gantt: {
            trackHeight: 30
          }
        };

        //Make a diagram link with html
        const chart = new google.visualization.Gantt(document.getElementById('ganttDiagram'));
        //Display the diagram with data and options parameters
        chart.draw(data, options);
    })


    /**
     * Delete task
     */
    socket.on('closeDelete', function(task)
    {
        //After DB deleting hide the modal
        const modal            = document.getElementsByClassName('modal');
        modal[4].style.display = "none";
        //reset fields
        document.getElementById("deleteTaskName").value = "";
        //reload the page to actualize the display
        document.location.reload(true);
    })






  }