/**
 * In this File we are catching in js every fields
 * to treat them into the rest of the program
 * 
 * --> Link server and IHM
 */

'use strict';

const socket      = io();
const nameService = "Gantt Productions";  // Group name

/**
 * Create task fields
 */
$('#createTask').submit(function(event){
    event.preventDefault(); //Prevents the submission of the task creation form

    let taskName        = document.getElementById("createTaskName").value;   // Get task name 
    let startDate       = document.getElementById("createStartDate").value;  // Get task start date
    let endDate         = document.getElementById("createEndDate").value;    // Get task end date
    let percentComplete = document.getElementById("createPercent").value;    // Get task percent
    let description     = document.getElementById("createDesc").value;       // Get task percent

    //Emit a task with this model
    socket.emit('newTask', 
    {
        nameService: nameService,
        projects   : [
            {
                name        : "Projet de test",
                desc        : "Description du projet, blablabla...",
                daysOff     : { Mo : true, Tu : true,  We : true, Th : true, Fr : true, Sa : false, Su : false },
                workingHours: { start : moment().hour(), end : moment().hour() },                                                                                                                                                   //moment() to manipulate and display dates and times 
                task        : [{ id: 0, name : taskName, desc : description, start : startDate, end : endDate, percentageProgress : percentComplete, color  : "#fc0202", linkedTask : [], resources : ['Christian / Ludovic'] }],
                groupTask   : [{ name : "optional", start : Date.now(), end : Date.now() }],
                resources   : [{ name : "Christian / Ludovic", cost : 10000, classe : 'Nains'}],
                milestones  : [{ name : "jalon °1", date : Date.now() }]
            }  
        ]
    });
})

socket.on('reCreate', function(data)
{
    console.log("data ==> " + data);

    let taskName        = data[0];
    let startDate       = data[1];
    let endDate         = data[2];
    let percentComplete = data[3];
    let description     = data[4];

    //Emit a task with this model
    socket.emit('newTask', 
    {
        nameService: nameService,
        projects   : [
            {
                name        : "Projet de test",
                desc        : "Description du projet, blablabla...",
                daysOff     : { Mo : true, Tu : true,  We : true, Th : true, Fr : true, Sa : false, Su : false },
                workingHours: { start : moment().hour(), end : moment().hour() },                                                                                                                                                   //moment() to manipulate and display dates and times 
                task        : [{ id: 0, name : taskName, desc : description, start : startDate, end : endDate, percentageProgress : percentComplete, color  : "#fc0202", linkedTask : [], resources : ['Christian / Ludovic'] }],
                groupTask   : [{ name : "optional", start : Date.now(), end : Date.now() }],
                resources   : [{ name : "Christian / Ludovic", cost : 10000, classe : 'Nains'}],
                milestones  : [{ name : "jalon °1", date : Date.now() }]
            }  
        ]
    });

    // socket.emit('removeTask', 
    // {
    //     taskName: data[0]
    // });

    document.location.reload(true);
})

/**
 * Update task fields
 */
$('#updateTask').submit(function(event){
    event.preventDefault(); //Prevents the submission of the task update form

    let taskName        = document.getElementById("updateTaskName").value;   // Get task name
    let startDate       = document.getElementById("updateStartDate").value;  // Get task start date
    let endDate         = document.getElementById("updateEndDate").value;    // Get task end date
    let percentComplete = document.getElementById("updatePercent").value;    // Get task percent
    let description     = document.getElementById("updateDesc").value;       // Get task percent

    //Same form as the creating model
    socket.emit('changeTask', 
    {
        nameService: nameService,
        projects   : [
            {
                name        : "Projet de test",
                desc        : "Description du projet, blablabla...",
                daysOff     : { Mo : true, Tu : true,  We : true, Th : true, Fr : true, Sa : false, Su : false },
                workingHours: { start : moment().hour(), end : moment().hour() },                                                                                                                             //moment() to manipulate and display dates and times 
                task        : [{ id: 0, name : taskName, desc : description, start : startDate, end : endDate, percentageProgress :percentComplete, color  : "#fc0202", linkedTask : [], resources : [] }],
                groupTask   : [{ name : "optional", start : Date.now(), end : Date.now() }],
                resources   : [{ name : "Jérémy", cost : 500, type : "humain" }],
                milestones  : [{ name : "jalon °1", date : Date.now() }]
            }  
        ]
    });
})


/**
 * List task fields
 */
$('#listTask').submit(function(event){
    event.preventDefault(); //Prevents the submission of the task list form

    //Only need taskName
    let taskName = document.getElementById("listTaskName").value;

    //Task name emission --> socket.js
    socket.emit('listATask', 
    {
        taskName: taskName
    })
})

/**
 * Delete task fields
 */
$('#deleteTask').submit(function(event){
    event.preventDefault(); //Prevents the submission of the task delete form

    //Only need the taskName value
    let taskName = document.getElementById("deleteTaskName").value;

    //Task name emission --> socket.js
    socket.emit('removeTask', 
    {
        taskName: taskName
    });
})

/**
 * Project parameters fields 
 */
$('#Parameters').submit(function(event){
    event.preventDefault(); //Prevents the submission of the task delete form

    //Catch every fields of the project parameter modal
    let projectName      = document.getElementById("paramTaskName").value;
    let monday           = document.getElementById("monday").checked;
    let tuesday          = document.getElementById("tuesday").checked;
    let wednesday        = document.getElementById("wednesday").checked;
    let thursday         = document.getElementById("thursday").checked;
    let friday           = document.getElementById("friday").checked;
    let saturday         = document.getElementById("saturday").checked;
    let sunday           = document.getElementById("sunday").checked;
    let startingHour     = document.getElementById("startingHour").value;
    let startingMinute   = document.getElementById("startingMinute").value;
    let endingHour       = document.getElementById("endingHour").value;
    let endingMinute     = document.getElementById("endingMinute").value;
    let projectResources = document.getElementById("projectResources").value;
    let paramDesc        = document.getElementById("paramDesc").value;


    //Emit parameters respecting the task model
    socket.emit('configureProject', 
    {
        nameService: nameService,
        projects   : [
            {
                name        : projectName,
                desc        : paramDesc,
                daysOff     : { Mo : monday, Tu : tuesday,  We : wednesday, Th : thursday, Fr : friday, Sa : saturday, Su : sunday},
                workingHours: { start : moment().hour(), end : moment().hour() },                                                                                       //moment() to manipulate and display dates and times 
                task        : [{ id: 0, name : "", desc : "", start : "", end : "", percentageProgress :  "", color  : "#fc0202", linkedTask : [], resources : [] }],
                groupTask   : [{ name : "optional", start : Date.now(), end : Date.now() }],
                resources   : [{ name : projectResources, cost : 500, classe : "humain" }],
                milestones  : [{ name : "jalon °1", date : Date.now() }]
            }  
        ]
    });
})
