/**
 * In this File we are treating the modal display & close
 */
    // Get modals
    const modal = document.getElementsByClassName('modal');
    // Get the button that opens the modal
    const btn = document.getElementsByClassName("button");
    // Get the <span> element that closes the modal
    const span = document.getElementsByClassName("close");


    // When the user clicks the button, open the modal 
    btn[0].onclick = function() {
        modal[0].style.display = "block";
    }

    btn[1].onclick = function() {
        modal[1].style.display = "block";
    }

    btn[2].onclick = function() {
        modal[2].style.display = "block";
    }
    
    btn[3].onclick = function() {
        modal[3].style.display = "block";
    }
    
    btn[4].onclick = function() {
        modal[4].style.display = "block";
    }

    // When the user clicks on X, close the modal
    span[0].onclick = function() {
        modal[0].style.display = "none";
    }

    span[1].onclick = function() {
        modal[1].style.display = "none";
    }

    span[2].onclick = function() {
        modal[2].style.display = "none";
    }

    span[3].onclick = function() {
        modal[3].style.display = "none";
    }

    span[4].onclick = function() {
        modal[4].style.display = "none";
    }
