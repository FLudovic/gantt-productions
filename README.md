# Gantt-Productions

A Node.js Project, creation of a Gantt Diagram.

To launch :

-   Prerequires Node.js, MongoDB database, A web Navigator (tested with firefox and chrome), an internet connection (for libraries), visual studio.

- Download the project
- Open it on visual studio code (or another IDE)
- Launch a new terminal
- Choose the project path using : cd /your path/
- Start the project using :     node server.js start

If erveything is okay you'll see :

listening on port 4000

    So you can try to connect from your web navigator using :  localhost:4000

Else if you have package errors check if all this packages are installed or write :

npm install body-parser --save
npm install express --save
npm install mongoose --save
npm install socket.io --save
npm install async --save



NB : If you are on the project some tasks will take time so let the program load correctly and don't spam it's useless
     if you have a display bug you need to place the cursor into the url and  type enter
     There is no display if there is no data in the database


