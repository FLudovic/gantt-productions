/**
 * In this File we establish the localhost server
 * Listening at port 4000
 */


//npm install body-parser --save
//npm install express --save
//npm install mongoose --save
//npm install socket.io --save
//npm install async --save

'use strict';

// Import libraries
const path       = require('path');
const bodyParser = require('body-parser');             // To handle post request
const express    = require('express');                 // Retrieve express
const app        = express();                          // Instancy express
const http       = require('http').createServer(app);  // Create server http by retrieving the express server


// Allows to attach text elements and allows to adapt the path to all os
// Dirname will search the folder from the absolute root to the file
app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "controllers")));

// Event server
const ServerEvent = require(path.join(__dirname, 'controllers', 'ServerEvent'));

require('./controllers/socket.js').listen(http, ServerEvent);

http.listen(4000, () => console.log('Listening on port 4000')); //Listening port 4000