/**
 * In this File we are listening at taskManager events
 * Depending on the case we are making some DB requests
 * as GET (& LIST), CREATE, DELETE, (UPDATE in progress...)
 * 
 * --> Link Server, Database and display returns (to ganttManager.js)
 */


'use strict';

//Define librairies
const socketio        = require('socket.io');  // socket.io is grafted to http so to the server 
const mongoose        = require('mongoose');   // Retrieve mongoose
const database        = 'Gantt-Productions';   // Database name
const taskCollections = 'tasks-collections';   //Collection name
const taskParameters  = 'tasks-parameters';    //Parameters collection name
const port            = 27017;                 //Port number 

// Define a mongoose Model declaration
const taskSchema = mongoose.Schema({
    nameService: String,
    projects   : [
        {
        name        : String,
        desc        : String,
        daysOff     : {Mo : Boolean, Tu : Boolean,  We : Boolean, Th : Boolean, Fr : Boolean, Sa : Boolean, Su : Boolean},
        workingHours: {start : Number, end : Number},
        task        : [{id: Number, name : String, desc : String , start : String, end : String, percentageProgress : String, color  : String, linkedTask : [], resources : [] }],
        groupTask   : [{ name : String, start : Number, end : Number}],
        resources   : [{ name : String, cost : Number, classe : String}],
        milestones  : [{ name : String, date : Number}]
        }
    ]
});


/**
 * Listening at server events
**/
 module.exports.listen = (server, ServerEvent) => {
    const io = socketio(server);

    io.on('connection', client => 
    {
        /**
         * Reception task
         */
        client.on('newTask', function (task) {
            //Creation of the Task model with association to the Mongo collection 'tasks-collections'.
            let Task = mongoose.model(taskCollections, taskSchema);
            //Mongo Connection with model parameters
            let db = mongoose.connection;
            db.once('open', function () {
                let newTask = new Task({
                    nameService: task.nameService,
                    projects   : [{
                        name        : task.projects[0].name,
                        desc        : task.projects[0].desc,
                        daysOff     : task.projects[0].daysOff,
                        workingHours: task.projects[0].workingHours,
                        task        : task.projects[0].task,
                        groupTask   : task.projects[0].groupTask,
                        resources   : task.projects[0].resources,
                        milestones  : task.projects[0].milestones
                    }]
                }) //Save Changes and disconnect
                newTask.save(function (err, task) {
                    console.log('New task save ' + newTask);
                    mongoose.disconnect();
                }); //Emit an event with newTask parameters --> ganttManager.js
                client.emit('clientEmitNewTask', newTask);
            });
            //Debug logs
            console.log('new task');
            console.log(task.projects[0].task);
            //Mongo connection path
            mongoose.connect('mongodb://localhost:' + port + "/" + database);
        })

        /**
         * Change task
         */
        client.on('changeTask', function (task) { 
                //Coding in progress
                let dataBaseGet = mongoose.connection;
                let getModel    = mongoose.model(taskCollections, taskSchema);

                console.log("Tache ===> " + task.projects[0].task[0].name);
                dataBaseGet.once('open', function () 
                {   //Getting all tasks query find(null) === SELECT *
                    let query = getModel.find({"projects.task.name" : task.projects[0].task[0].name });
                    query.lean().exec(function (err, docs) 
                    {
                        //Emit task --> ganttManager.js
                        if(docs == "")
                        {client.emit("UpdateNotFound");}
                        else{
                            let taskName = task.projects[0].task[0].name;
                            let startDate = task.projects[0].task[0].start;
                            let endDate = task.projects[0].task[0].end;
                            let percentComplete = task.projects[0].task[0].percentageProgress;
                            let description = task.projects[0].task[0].desc;

                            if(startDate == ""){startDate = docs[0].projects[0].task[0].start;}
                            else{startDate = task.projects[0].task[0].start;}
                            if(endDate == ""){endDate = docs[0].projects[0].task[0].end;}
                            else{endDate = task.projects[0].task[0].end;}
                            if(percentComplete == 0){percentComplete = docs[0].projects[0].task[0].percentageProgress;}
                            else{percentComplete = task.projects[0].task[0].percentageProgress;}
                            if(description == ""){description = docs[0].projects[0].task[0].desc;}
                            else{description = task.projects[0].task[0].desc;}

                            // console.log("startDate ==>" + startDate);
                            // console.log("endDate ==>" + endDate);
                            // console.log("percentComplete ==>" + percentComplete);
                            // console.log("description ==>" + description);
                            let data = [taskName, startDate, endDate, parseInt(percentComplete, 10), description];
                            client.emit("reCreate", data);
                            
                        }
                        mongoose.disconnect();
                       
                    });     
                });
                //Mongo connection path
                mongoose.connect('mongodb://localhost:' + port + "/" + database);
        })

        /**
         * List task
         */
        client.on('listATask', function(task){

            let db   = mongoose.connection;
            let Task = mongoose.model(taskCollections, taskSchema);

            //Display the sent taskName
            console.log(task.taskName);
            db.once('open', function () {

                //Create a query to find every tasks
                let query = Task.find(null);
                query.where({"projects.task.name" : task.taskName });

                //lean() allows to get a mongo object and not a document 
                query.lean().exec(function(err, task){
                    //Error case

                    if (err) {throw err;}
                    else{ //Emit task --> ganttManager.js
                        client.emit('clientEmitListTask', task);
                        //Mongo disconnect
                        mongoose.disconnect();
                    }
                })
            });
            //Mongo connection path
            mongoose.connect('mongodb://localhost:' + port + "/" + database);
        })
    
        /**
         * Remove task
         */
        client.on('removeTask', function (task) {
    
            let db   = mongoose.connection;
            let Task = mongoose.model(taskCollections, taskSchema);

            db.once('open', function () 
            { //Debug log display the path
            console.log("CHEMIN ==> " + task.taskName );
            //Find object with the path name and delete it
            Task.findOneAndDelete({ "projects.task.name" : task.taskName }, function (err) {
                if(err) console.log(err);
                console.log("Successful deletion");
                //Emit an event to the gantManager.js (to hide the modal) 
                client.emit('closeDelete');
                mongoose.disconnect();
              });

            });
            //Mongo connection path
            mongoose.connect('mongodb://localhost:' + port + "/" + database);
        })  

        /**
         * Get Tasks
         */
        let dataBaseGet = mongoose.connection;
        let getModel    = mongoose.model(taskCollections, taskSchema);
        //debug log
        console.log('Dans le GET task');
        dataBaseGet.once('open', function () 
        {   //Getting all tasks query find(null) === SELECT *
            let query = getModel.find(null);
            query.lean().exec(function (err, docs) {
                console.log("Get Socket : " + docs);
                //Emit tasks to display on connection --> ganttManger.js
                client.emit('getTasks', docs);
                mongoose.disconnect();
            });     
        });
        //Mongo connection path
        mongoose.connect('mongodb://localhost:' + port + "/" + database);
        


    })  
}