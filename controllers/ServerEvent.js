'use strict';

// Event server
let EventEmitter = require('events').EventEmitter;
let ServerEvent = new EventEmitter();

module.exports = ServerEvent;